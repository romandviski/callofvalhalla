// Copyright ICFHAG Studio. All rights reserved. 2021

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerState.h"
#include "BasePlayerState.generated.h"

/**
 * 
 */
UCLASS()
class CALLOFVALHALLA_API ABasePlayerState : public APlayerState
{
	GENERATED_BODY()
	
};
