// Copyright ICFHAG Studio. All rights reserved. 2021


#include "Game/GameObject/COVEnviromentActorBase.h"
#include "Materials/MaterialInterface.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "../Public/COVStateEffect.h"
#include "Net/UnrealNetwork.h"
#include "Kismet/GameplayStatics.h"
#include "Particles/ParticleSystemComponent.h"
#include "Engine/ActorChannel.h"
#include "../Public/FuncLibrary/Types.h"
// Sets default values
ACOVEnviromentActorBase::ACOVEnviromentActorBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	bReplicates = true;
}

// Called when the game starts or when spawned
void ACOVEnviromentActorBase::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ACOVEnviromentActorBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

EPhysicalSurface ACOVEnviromentActorBase::GetSurfaceType()
{
	EPhysicalSurface SurfaceResult = EPhysicalSurface::SurfaceType_Default;
	UStaticMeshComponent* myMesh = Cast<UStaticMeshComponent>(GetComponentByClass(UStaticMeshComponent::StaticClass()));
	if (myMesh)
	{
		UMaterialInterface* myMaterial = myMesh->GetMaterial(0);
		if (myMaterial)
		{
			SurfaceResult = myMaterial->GetPhysicalMaterial()->SurfaceType;

		}
	}
	return SurfaceResult;

}

TArray<UCOVStateEffect*> ACOVEnviromentActorBase::GetAllCurrentEffects()
{
	return Effects;
}

void ACOVEnviromentActorBase::RemoveEffect(UCOVStateEffect* RemoveEffect)
{
	Effects.Remove(RemoveEffect);
	if (!RemoveEffect->bAutoDestroy)
	{
		SwitchEffect(RemoveEffect, false);
		EffectRemove = RemoveEffect;
	}
}


void ACOVEnviromentActorBase::AddEffect(UCOVStateEffect* newEffect)
{
	Effects.Add(newEffect);
	if (!newEffect->bAutoDestroy)
	{
		SwitchEffect(newEffect, true);
		EffectAdd = newEffect;
	}
	else
	{
		if (newEffect->ParticleEffect)
		{
			ExecuteEffectAdded_OnServer(newEffect->ParticleEffect);
		}
	}
}

void ACOVEnviromentActorBase::ExecuteEffectAdded_OnServer_Implementation(UParticleSystem* ExecuteFX)
{
	ExecuteEffectAdded_Multicast(ExecuteFX);
}

void ACOVEnviromentActorBase::ExecuteEffectAdded_Multicast_Implementation(UParticleSystem* ExecuteFX)
{
	UTypes::ExecuteEffectAdded(ExecuteFX, this, EffectOffset, NAME_None);
}

void ACOVEnviromentActorBase::GetInfoToAttachEmiterEffect(USceneComponent*& Component, FVector(&Offset), FName& AttachedBone)
{
	UStaticMeshComponent* Mesh = Cast<UStaticMeshComponent>(FindComponentByClass(UStaticMeshComponent::StaticClass()));
	if (Mesh)
	{
		Component = Mesh;
		Offset = Mesh->GetRelativeLocation() + EffectOffset;
		AttachedBone = "NONE";
	}
}


void ACOVEnviromentActorBase::EffectAdd_OnRep()
{
	if (EffectAdd)
	{
		SwitchEffect(EffectAdd, true);
	}
}

void ACOVEnviromentActorBase::EffectRemove_OnRep()
{
	if (EffectRemove)
	{
		SwitchEffect(EffectRemove, false);
	}
}
void ACOVEnviromentActorBase::SwitchEffect(UCOVStateEffect* Effect, bool bAddOrRemove)
{
	if (bAddOrRemove)
	{
		if (Effect && Effect->ParticleEffect)
		{
			FName BoneToAttached = NAME_None;
			FVector Loc = EffectOffset;
			USceneComponent* MeshRoot = GetRootComponent();
			if (MeshRoot)
			{
				UParticleSystemComponent* Partical = UGameplayStatics::SpawnEmitterAttached(Effect->ParticleEffect, MeshRoot, BoneToAttached, Loc, FRotator::ZeroRotator, Effect->ScaleEmitter, EAttachLocation::SnapToTarget, false);
				ParticalEffects.Add(Partical);
			}
		}
	}
	else
	{
		if (ParticalEffects.Num() > 0)
		{
			int32 i = 0;
			bool bIsFind = false;
			while (i < ParticalEffects.Num() && !bIsFind)
			{
				if (ParticalEffects[i]->Template && Effect->ParticleEffect && Effect->ParticleEffect == ParticalEffects[i]->Template)
				{
					bIsFind = true;
					ParticalEffects[i]->DeactivateSystem();
					ParticalEffects[i]->DestroyComponent();
					ParticalEffects.RemoveAt(i);
				}
				i++;
			}
		}
		
	}
}

bool ACOVEnviromentActorBase::ReplicateSubobjects(UActorChannel* Channel, FOutBunch* Bunch, FReplicationFlags* RepFlags)
{
	bool WroteSomething = Super::ReplicateSubobjects(Channel, Bunch, RepFlags);

	for (int32 i = 0; i < Effects.Num(); i++)
	{
		if (Effects[i])
		{
			WroteSomething |= Channel->ReplicateSubobject(Effects[i], *Bunch, *RepFlags);	// (this makes those subobjects 'supported', and from here on those objects may have reference replicated)		
		}
	}
	return WroteSomething;
}
void ACOVEnviromentActorBase::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(ACOVEnviromentActorBase, Effects);
	DOREPLIFETIME(ACOVEnviromentActorBase, EffectAdd);
	DOREPLIFETIME(ACOVEnviromentActorBase, EffectRemove);
}
