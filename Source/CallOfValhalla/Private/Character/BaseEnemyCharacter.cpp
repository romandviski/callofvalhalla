// Copyright ICFHAG Studio. All rights reserved. 2021


#include "Character/BaseEnemyCharacter.h"
#include "../Public/Character/COVHealthComponent.h"



ABaseEnemyCharacter::ABaseEnemyCharacter()
{
	HealthComponent = CreateDefaultSubobject<UCOVHealthComponent>(TEXT("HealthComponent"));
	if (HealthComponent)
	{
		HealthComponent->OnDead.AddDynamic(this, &ABaseEnemyCharacter::DeathCharacter);
	}
}
void ABaseEnemyCharacter::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

}
void ABaseEnemyCharacter::DeathCharacter()
{
	Super::DeathCharacter();
}

void ABaseEnemyCharacter::BeginPlay()
{
	Super::BeginPlay();
}

