// Copyright ICFHAG Studio. All rights reserved. 2021


#include "COVInterfaceGameActor.h"


// Add default functionality here for any ICOVInterfaceGameActor functions that are not pure virtual.

EPhysicalSurface ICOVInterfaceGameActor::GetSurfaceType()
{
	return EPhysicalSurface::SurfaceType_Default;
}
TArray<UCOVStateEffect*> ICOVInterfaceGameActor::GetAllCurrentEffects()
{
	TArray<UCOVStateEffect*> Effect;
	return Effect;
}

void ICOVInterfaceGameActor::RemoveEffect(UCOVStateEffect* RemoveEffect)
{

}

void ICOVInterfaceGameActor::AddEffect(UCOVStateEffect* newEffect)
{

}

void ICOVInterfaceGameActor::GetInfoToAttachEmiterEffect(USceneComponent*& Component, FVector(&Offset), FName& AttachedBone)
{
}

