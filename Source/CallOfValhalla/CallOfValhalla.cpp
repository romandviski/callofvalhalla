// Copyright ICFHAG Studio. All rights reserved. 2021

#include "CallOfValhalla.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, CallOfValhalla, "CallOfValhalla" );

DEFINE_LOG_CATEGORY(LogCOV)
DEFINE_LOG_CATEGORY(LogCOV_Net)